package Util;

import javax.swing.*;
import java.io.*;

/**
 * Created by sergio on 13/11/2015.
 */
public class Util {


    //Mensaje para avisos importantes después de realizar una acción
    public static void MensajeDeAlerta(String Mensaje) {
        JOptionPane.showMessageDialog(null, Mensaje);
    }

    //Mensaje para confirmar si está seguro de querer realizar una acción
    public static int MensajeDeConfirmacion(String Mensaje){
        return JOptionPane.showConfirmDialog(null, Mensaje);
    }

    //Método que será llamado cada vez que se realicen cambios sobre un vector determinado y por lo tanto se deseé guardarlo
    public static void GuardarFichero(Object Objeto, String NombreDelFichero) {

        ObjectOutputStream serializador = null;

        try {
            serializador = new ObjectOutputStream(new FileOutputStream(NombreDelFichero));
            serializador.writeObject(Objeto);
            serializador.close();
        } catch (IOException error) {
            {
                error.printStackTrace();
            }

        }
    }

    //Método para cargar un vector determinado
    public static Object CargarFichero(String NombreDelFichero){

        ObjectInputStream desserializador = null;
        FileInputStream fichero = null;

        try {
            fichero = new FileInputStream(NombreDelFichero);

            try {
                desserializador = new ObjectInputStream(fichero);
            } catch (IOException error) {
                error.printStackTrace();
            }

            try {
                return desserializador.readObject();
            } catch (IOException error){
            error.printStackTrace();
            return null;

        } catch (ClassNotFoundException error) {
                error.printStackTrace();
                return null;
            }

        } catch (FileNotFoundException error){
            return null;
        }

    }

}
