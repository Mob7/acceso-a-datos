package Base;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by sergio on 12/11/2015.
 */

//Clase disco, contiene cada dato de la pestaña Disco y es Serializable para poder convertirlo a bytes
public class Disco implements Serializable {

    private String tituloDisco;
    private Artista artistas;
    private int numeroCopiasVendidas;
    private float precioSalida;
    private Discografica companias ;
    private Date fechaDeSalida;

    //Constructor para facilitar la creación del objeto de forma que será invocado automáticamente y necesitará obligatoriamente estos datos
    public Disco (String tituloDisco, Artista artistas, int numeroCopiasVendidas, float precioSalida, Discografica companias, Date fechaDeSalida){

        this.tituloDisco = tituloDisco;
        this.artistas = artistas;
        this.numeroCopiasVendidas = numeroCopiasVendidas;
        this.precioSalida = precioSalida;
        this.companias = companias;
        this.fechaDeSalida = fechaDeSalida;

    }

    //Getters y setters de cada uno de los datos
    public String getTituloDisco() {
        return tituloDisco;
    }

    public void setTituloDisco(String tituloDisco) {
        this.tituloDisco = tituloDisco;
    }

    public Artista getArtistas() {
        return artistas;
    }

    public void setArtistas(Artista listadeartistas) {
        this.artistas = artistas;
    }

    public int getNumeroCopiasVendidas() {
        return numeroCopiasVendidas;
    }

    public void setNumeroCopiasVendidas(int numeroCopiasVendidas) {
        this.numeroCopiasVendidas = numeroCopiasVendidas;
    }

    public float getPrecioSalida() {
        return precioSalida;
    }

    public void setPrecioSalida(float precioSalida) {
        this.precioSalida = precioSalida;
    }

    public Discografica getCompanias() {
        return companias;
    }

    public void setCompanias(Discografica companias) {
        this.companias = companias;
    }

    public Date getFechaDeSalida() {
        return fechaDeSalida;
    }

    public void setFechaDeSalida(Date fechaDeSalida) {
        this.fechaDeSalida = fechaDeSalida;
    }

    //Método que representará a cada objeto creado, en este caso en el vector sólo se mostrará el título
    public String toString(){
        return tituloDisco;
    }

}
