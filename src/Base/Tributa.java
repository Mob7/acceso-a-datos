package Base;

/**
 * Created by sergio on 12/11/2015.
 */

//Clase creada única y exclusivamente para los RaddioButton de la discográfica
//en los que querremos dejar constancia de si tributa o no en España
public enum Tributa {

    Si, No

}
