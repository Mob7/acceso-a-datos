package Base;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by sergio on 12/11/2015.
 */

//Clase artista, contiene cada dato de la pestaña Artista y es Serializable para poder convertirlo a bytes
public class Artista implements Serializable {

    private String nombreArtistico;
    private String nombreOriginal;
    private int numeroTelefono;
    private float precioPorActuacion;
    private String ciudadNacimiento;
    private Date fechaNacimiento;

    //Constructor para facilitar la creación del objeto de forma que será invocado automáticamente y necesitará obligatoriamente estos datos
    public Artista (String nombreArtistico, String nombreOriginal, int numeroTelefono, float precioPorActuacion, String ciudadNacimiento, Date fechaNacimiento){

        this.nombreArtistico = nombreArtistico;
        this.nombreOriginal = nombreOriginal;
        this.numeroTelefono = numeroTelefono;
        this.precioPorActuacion = precioPorActuacion;
        this.ciudadNacimiento = ciudadNacimiento;
        this.fechaNacimiento = fechaNacimiento;

    }

    //Getters y setters de cada uno de los datos
    public String getNombreArtistico() {
        return nombreArtistico;
    }

    public void setNombreArtistico(String nombreArtistico) {
        this.nombreArtistico = nombreArtistico;
    }

    public String getNombreOriginal() {
        return nombreOriginal;
    }

    public void setNombreOriginal(String nombreOriginal) {
        this.nombreOriginal = nombreOriginal;
    }

    public int getNumeroTelefono() {
        return numeroTelefono;
    }

    public void setNumeroTelefono(int numeroTelefono) {
        this.numeroTelefono = numeroTelefono;
    }

    public float getPrecioPorActuacion() {
        return precioPorActuacion;
    }

    public void setPrecioPorActuacion(float precioPorActuacion) {
        this.precioPorActuacion = precioPorActuacion;
    }

    public String getCiudadNacimiento() {
        return ciudadNacimiento;
    }

    public void setCiudadNacimiento(String ciudadNacimiento) {
        this.ciudadNacimiento = ciudadNacimiento;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    //Método que representará a cada objeto creado, en este caso en el vector sólo se mostrará el nombre
    public String toString(){
        return nombreArtistico;
    }
}
