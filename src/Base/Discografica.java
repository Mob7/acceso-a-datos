package Base;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by sergio on 12/11/2015.
 */

//Clase discográfica, contiene cada dato de la pestaña Discográfica y es Serializable para poder convertirlo a bytes
public class Discografica implements Serializable {

    private String nombreDiscografica;
    private int numeroArtistasContratados;
    private String ciudadSede;
    private float dineroRecaudado;
    private Tributa tributa;
    private Artista artistaEstrella;
    private Date fechaRegistro;

    //Constructor para facilitar la creación del objeto de forma que será invocado automáticamente y necesitará obligatoriamente estos datos
    public Discografica(String nombreDiscografica, int numeroArtistasContratados, String ciudadSede, float dineroRecaudado, Tributa tributa, Artista artistaEstrella, Date fechaRegistro){

        this.nombreDiscografica = nombreDiscografica;
        this.numeroArtistasContratados = numeroArtistasContratados;
        this.ciudadSede = ciudadSede;
        this.dineroRecaudado = dineroRecaudado;
        this.tributa = tributa;
        this.artistaEstrella = artistaEstrella;
        this.fechaRegistro = fechaRegistro;

    }

    //Getters y setters de cada uno de los datos
    public String getNombreDiscografica() {
        return nombreDiscografica;
    }

    public void setNombreDiscografica(String nombreDiscografica) {
        this.nombreDiscografica = nombreDiscografica;
    }

    public int getNumeroArtistasContratados() {
        return numeroArtistasContratados;
    }

    public void setNumeroArtistasContratados(int numeroArtistasContratados) {
        this.numeroArtistasContratados = numeroArtistasContratados;
    }

    public String getCiudadSede() {
        return ciudadSede;
    }

    public void setCiudadSede(String ciudadSede) {
        this.ciudadSede = ciudadSede;
    }

    public float getDineroRecaudado() {
        return dineroRecaudado;
    }

    public void setDineroRecaudado(float dineroRecaudado) {
        this.dineroRecaudado = dineroRecaudado;
    }

    public Tributa getTributa() {
        return tributa;
    }

    public void setTributa(Tributa tributa) {
        this.tributa = tributa;
    }

    public Artista getArtistaEstrella() {
        return artistaEstrella;
    }

    public void setArtistaEstrella(Artista artistaEstrella) {
        this.artistaEstrella = artistaEstrella;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    //Método que representará a cada objeto creado, en este caso en el vector sólo se mostrará el nombre
    public String toString(){
        return nombreDiscografica;
    }

}
