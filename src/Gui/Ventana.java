package Gui;

import Base.Artista;
import Base.Disco;
import Base.Discografica;
import Base.Tributa;
import Util.Util;
import com.toedter.calendar.JDateChooser;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * Created by sergio on 12/11/2015.
 */
public class Ventana {
    private JPanel panel1;
    private JTabbedPane tabbedPane1;
    private JTextField tfArtistaNombre;
    private JTextField tfArtistaNombreOriginal;
    private JTextField tfArtistaNumeroTlfno;
    private JTextField tfArtistaPrecioActuacion;
    private JTextField tfArtistaCiudadNacimiento;
    private JTextField tfDiscoTitulo;
    private JTextField tfDiscoCopiasVendidas;
    private JTextField tfDiscoPrecioSalida;
    private JTextField tfDiscograficaNombre;
    private JTextField tfDiscograficaNumeroArtistas;
    private JTextField tfDiscograficaCiudadSede;
    private JTextField tfDiscograficaDineroRecaudado;
    private JComboBox cbDiscoCompania;
    private JRadioButton rbDiscograficaSiTributa;
    private JRadioButton rbDiscograficaNoTributa;
    private JComboBox cbDiscograficaArtistaEstrella;
    private JList listDiscografica;
    private JButton btDiscograficaModificar;
    private JButton btDiscograficaNuevo;
    private JButton btDiscograficaEliminar;
    private JButton btDiscograficaGuardar;
    private JList listDisco;
    private JButton btDiscoModificar;
    private JButton btDiscoNuevo;
    private JButton btDiscoGuardar;
    private JButton btDiscoEliminar;
    private JList listArtista;
    private JButton btArtistaModificar;
    private JButton btArtistaNuevo;
    private JButton btArtistaEliminar;
    private JButton btArtistaGuardar;
    private JLabel lbArtistaNombreArtistico;
    private JLabel lbArtistaNombreOriginal;
    private JLabel lbArtistaNumeroTelefono;
    private JLabel lbArtistaPrecioActuacion;
    private JLabel lbArtistaCiudadNacimiento;
    private JLabel lbArtistaFechaNacimiento;
    private JLabel lbDiscoTitulo;
    private JLabel lbDiscoArtista;
    private JComboBox cbDiscoArtista;
    private JLabel lbDiscoCopiasVendidas;
    private JLabel lbDiscoPrecio;
    private JLabel lbDiscoCompañia;
    private JLabel lbDiscoFechaSalida;
    private JLabel lbDiscograficaNombre;
    private JLabel lbDiscograficaCiudadSede;
    private JLabel lbDiscograficaRecaudacion;
    private JLabel lbDiscograficaTributa;
    private JLabel lbDiscograficaFechaRegistro;
    private JLabel lbDiscograficaArtistaEstrella;
    private JLabel lbDiscograficaArtista;
    private JDateChooser calDiscograficaFechaRegistro;
    private JDateChooser calArtistaFechaNacimiento;
    private JDateChooser calDiscoFechaSalida;

    public static boolean AutoGuardado;
    public static String Separador;
    public static String Ruta;

    private boolean nuevoArtista;
    private boolean nuevoDisco;
    private boolean nuevoDiscografica;

    private enum Tipo {
        ARTISTA, DISCO, DISCOGRAFICA
    }

    private ArrayList<Artista> VectorArtistas;
    private ArrayList<Disco> VectorDiscos;
    private ArrayList<Discografica> VectorDiscograficas;

    public Ventana() {

        //Establezco el separador para que sea compatible con cualquier sistema operativo
        Separador = System.getProperty("file.separator");

        //Indico la ruta en la que se encuentran los datos almacenados y lo relaciono con el objeto
        Ruta = "";
        Object cargarArtista = Util.CargarFichero(Ruta+"Vectores"+Separador+"Artistas.obj");
        Object cargarDiscograficas = Util.CargarFichero(Ruta+"Vectores"+Separador+"Discograficas.obj");
        Object cargarDiscos = Util.CargarFichero(Ruta+"Vectores"+Separador+"Discos.obj");

        //Cargo los vectores al inicializar el programa y se los asigno al objeto; en el caso de que estén vacíos creo unos nuevos
        if(cargarArtista != null){
            VectorArtistas = (ArrayList<Artista>)cargarArtista;
            actualizarLista(Tipo.ARTISTA);
        } else {
            VectorArtistas = new ArrayList<Artista>();
        }

        if(cargarDiscograficas != null){
            VectorDiscograficas = (ArrayList<Discografica>)cargarDiscograficas;
            actualizarLista(Tipo.DISCOGRAFICA);
        } else {
            VectorDiscograficas = new ArrayList<Discografica>();
        }

        if(cargarDiscos !=null){
            VectorDiscos = (ArrayList<Disco>)cargarDiscos;
            actualizarLista(Tipo.DISCO);
        } else {
            VectorDiscos = new ArrayList<Disco>();
        }

        nuevoArtista = true;
        nuevoDiscografica = true;
        nuevoDisco = true;

        //Establezco el listener de cada JList para que al seleccionarlos se carguen sus datos correspondientes
        listArtista.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                int Indice = listArtista.getSelectedIndex();
                if(Indice > -1){
                    cargarDatosSeleccionados(Indice, Tipo.ARTISTA);
                }
                btArtistaModificar.setEnabled(true);
                btArtistaEliminar.setEnabled(true);
            }
        });

        listDiscografica.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                int Indice = listDiscografica.getSelectedIndex();
                if(Indice > -1){
                    cargarDatosSeleccionados(Indice, Tipo.DISCOGRAFICA);
                }
                btDiscograficaModificar.setEnabled(true);
                btDiscograficaEliminar.setEnabled(true);
            }
        });

        listDisco.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                int Indice = listDisco.getSelectedIndex();
                if(Indice > -1){
                    cargarDatosSeleccionados(Indice, Tipo.DISCO);
                }
                btDiscoModificar.setEnabled(true);
                btDiscoEliminar.setEnabled(true);
            }
        });


        //Inicializo los listener de cada botón
        btArtistaNuevo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                activarCajas(Tipo.ARTISTA, nuevoArtista=true);
            }
        });

        btDiscograficaNuevo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                activarCajas(Tipo.DISCOGRAFICA, nuevoDiscografica=true);
            }
        });

        btDiscoNuevo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                activarCajas(Tipo.DISCO, nuevoDisco=true);
            }
        });

        btArtistaGuardar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                guardarDatos(Tipo.ARTISTA, nuevoArtista);
            }
        });

        btDiscograficaGuardar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                guardarDatos(Tipo.DISCOGRAFICA, nuevoDiscografica);
            }
        });

        btDiscoGuardar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                guardarDatos(Tipo.DISCO, nuevoDisco);
            }
        });

        btArtistaModificar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                modificarDatos(Tipo.ARTISTA);
            }
        });

        btDiscograficaModificar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                modificarDatos(Tipo.DISCOGRAFICA);
            }
        });

        btDiscoModificar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                modificarDatos(Tipo.DISCO);
            }
        });

        btArtistaEliminar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                eliminarDatos(Tipo.ARTISTA, listArtista.getSelectedIndex());
            }
        });

        btDiscograficaEliminar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                eliminarDatos(Tipo.DISCOGRAFICA, listDiscografica.getSelectedIndex());
            }
        });

        btDiscoEliminar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                eliminarDatos(Tipo.DISCO, listDisco.getSelectedIndex());
            }
        });

    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Ventana");
        frame.setContentPane(new Ventana().panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    //Mediante este método, al seleccionar un objeto del vector se cargarán sus datos en las cajas correspondientes
    public void cargarDatosSeleccionados(int Indice, Tipo tipo){
        switch (tipo){

            case ARTISTA:
                tfArtistaNombreOriginal.setText(VectorArtistas.get(Indice).getNombreOriginal());
                tfArtistaNombre.setText(VectorArtistas.get(Indice).getNombreArtistico());
                tfArtistaNumeroTlfno.setText(String.valueOf(VectorArtistas.get(Indice).getNumeroTelefono()));
                tfArtistaPrecioActuacion.setText(String.valueOf(VectorArtistas.get(Indice).getPrecioPorActuacion()));
                tfArtistaCiudadNacimiento.setText(VectorArtistas.get(Indice).getCiudadNacimiento());
                calArtistaFechaNacimiento.setDate(VectorArtistas.get(Indice).getFechaNacimiento());

                seleccionarEditable(Tipo.ARTISTA, false);

                break;

            case DISCOGRAFICA:
                tfDiscograficaNombre.setText(VectorDiscograficas.get(Indice).getNombreDiscografica());
                tfDiscograficaNumeroArtistas.setText(String.valueOf(VectorDiscograficas.get(Indice).getNumeroArtistasContratados()));
                tfDiscograficaCiudadSede.setText(VectorDiscograficas.get(Indice).getCiudadSede());
                tfDiscograficaDineroRecaudado.setText(String.valueOf(VectorDiscograficas.get(Indice).getDineroRecaudado()));
                if(VectorDiscograficas.get(Indice).getTributa() == Tributa.Si){
                    rbDiscograficaSiTributa.setSelected(true);
                    rbDiscograficaNoTributa.setSelected(false);
                } else {
                    rbDiscograficaSiTributa.setSelected(false);
                    rbDiscograficaNoTributa.setSelected(true);
                }
                cbDiscograficaArtistaEstrella.setSelectedItem(VectorDiscograficas.get(Indice).getArtistaEstrella());
                calDiscograficaFechaRegistro.setDate(VectorDiscograficas.get(Indice).getFechaRegistro());

                seleccionarEditable(Tipo.DISCOGRAFICA, false);

                break;

            case DISCO:
                tfDiscoTitulo.setText(VectorDiscos.get(Indice).getTituloDisco());
                cbDiscoArtista.setSelectedItem(VectorDiscos.get(Indice).getArtistas());
                tfDiscoCopiasVendidas.setText(String.valueOf(VectorDiscos.get(Indice).getNumeroCopiasVendidas()));
                tfDiscoPrecioSalida.setText(String.valueOf(VectorDiscos.get(Indice).getNumeroCopiasVendidas()));
                cbDiscoCompania.setSelectedItem(VectorDiscos.get(Indice).getCompanias());
                calDiscoFechaSalida.setDate(VectorDiscos.get(Indice).getFechaDeSalida());

                seleccionarEditable(Tipo.DISCO, false);

                break;
        }

    }

    //A través de este método configuraremos si las cajas podrán ser editables o no
    public void seleccionarEditable(Tipo tipo, boolean Sino){
        switch (tipo){

            case ARTISTA:
                tfArtistaNombre.setEditable(Sino);
                tfArtistaNombreOriginal.setEditable(Sino);
                tfArtistaNumeroTlfno.setEditable(Sino);
                tfArtistaPrecioActuacion.setEditable(Sino);
                tfArtistaCiudadNacimiento.setEditable(Sino);
                calArtistaFechaNacimiento.setEnabled(Sino);

                tfArtistaNombre.setEnabled(Sino);
                tfArtistaNombreOriginal.setEnabled(Sino);
                tfArtistaNumeroTlfno.setEnabled(Sino);
                tfArtistaPrecioActuacion.setEnabled(Sino);
                tfArtistaCiudadNacimiento.setEnabled(Sino);
                calArtistaFechaNacimiento.setEnabled(Sino);

                break;

            case DISCOGRAFICA:
                tfDiscograficaNombre.setEditable(Sino);
                tfDiscograficaNumeroArtistas.setEditable(Sino);
                tfDiscograficaCiudadSede.setEditable(Sino);
                tfDiscograficaDineroRecaudado.setEditable(Sino);
                rbDiscograficaSiTributa.setEnabled(Sino);
                rbDiscograficaNoTributa.setEnabled(Sino);
                cbDiscograficaArtistaEstrella.setEditable(Sino);
                calDiscograficaFechaRegistro.setEnabled(Sino);

                tfDiscograficaNombre.setEnabled(Sino);
                tfDiscograficaNumeroArtistas.setEnabled(Sino);
                tfDiscograficaCiudadSede.setEnabled(Sino);
                tfDiscograficaDineroRecaudado.setEnabled(Sino);
                rbDiscograficaSiTributa.setEnabled(Sino);
                rbDiscograficaNoTributa.setEnabled(Sino);
                cbDiscograficaArtistaEstrella.setEnabled(Sino);
                calDiscograficaFechaRegistro.setEnabled(Sino);

                break;

            case DISCO:
                tfDiscoTitulo.setEditable(Sino);
                cbDiscoArtista.setEditable(Sino);
                tfDiscoCopiasVendidas.setEditable(Sino);
                tfDiscoPrecioSalida.setEditable(Sino);
                cbDiscoCompania.setEditable(Sino);
                calDiscoFechaSalida.setEnabled(Sino);

                tfDiscoTitulo.setEnabled(Sino);
                cbDiscoArtista.setEnabled(Sino);
                tfDiscoCopiasVendidas.setEnabled(Sino);
                tfDiscoPrecioSalida.setEnabled(Sino);
                cbDiscoCompania.setEnabled(Sino);
                calDiscoFechaSalida.setEnabled(Sino);

                break;
        }

    }

    //Inicializamos las cajas y las dejamos en blanco, llamaremos a este método por ejemplo al pulsar el botón nuevo o al
    //eliminar un objeto del vector
    public void limpiarCajas(Tipo tipo){
        switch (tipo){

            case ARTISTA:
                tfArtistaNombre.setText("");
                tfArtistaNombreOriginal.setText("");
                tfArtistaNumeroTlfno.setText("");
                tfArtistaPrecioActuacion.setText("");
                tfArtistaCiudadNacimiento.setText("");
                calArtistaFechaNacimiento.setDate(null);
                nuevoArtista=true;

                break;

            case DISCOGRAFICA:
                    tfDiscograficaNombre.setText("");
                    tfDiscograficaNumeroArtistas.setText("");
                    tfDiscograficaCiudadSede.setText("");
                    tfDiscograficaDineroRecaudado.setText("");
                    rbDiscograficaSiTributa.setSelected(false);
                    rbDiscograficaNoTributa.setSelected(false);
                    calDiscograficaFechaRegistro.setDate(null);
                    nuevoDiscografica=true;

                break;

            case DISCO:
                    tfDiscoTitulo.setText("");
                    tfDiscoCopiasVendidas.setText("");
                    tfDiscoPrecioSalida.setText("");
                    calDiscoFechaSalida.setDate(null);
                    nuevoDisco=true;

                break;
        }


    }

    //Llamamos a este método siempre para habilitar o deshabilitar las cajas
    public void activarCajas(Tipo tipo, boolean nuevo){
        switch (tipo){

            case ARTISTA:
                if (nuevo){
                    limpiarCajas(Tipo.ARTISTA);
                }

                tfArtistaNombre.setEnabled(nuevo);
                tfArtistaNombreOriginal.setEnabled(nuevo);
                tfArtistaNumeroTlfno.setEnabled(nuevo);
                tfArtistaPrecioActuacion.setEnabled(nuevo);
                tfArtistaCiudadNacimiento.setEnabled(nuevo);
                calArtistaFechaNacimiento.setEnabled(nuevo);

                btArtistaNuevo.setEnabled(nuevo);
                btArtistaModificar.setEnabled(!nuevo);
                btArtistaEliminar.setEnabled(!nuevo);
                btArtistaGuardar.setEnabled(nuevo);

                seleccionarEditable(Tipo.ARTISTA, true);

                break;

            case DISCOGRAFICA:
                if (nuevo){
                    limpiarCajas(Tipo.DISCOGRAFICA);
                }

                tfDiscograficaNombre.setEnabled(nuevo);
                tfDiscograficaNumeroArtistas.setEnabled(nuevo);
                tfDiscograficaCiudadSede.setEnabled(nuevo);
                tfDiscograficaDineroRecaudado.setEnabled(nuevo);
                rbDiscograficaSiTributa.setEnabled(nuevo);
                rbDiscograficaNoTributa.setEnabled(nuevo);
                cbDiscograficaArtistaEstrella.setEnabled(nuevo);
                calDiscograficaFechaRegistro.setEnabled(nuevo);

                btDiscograficaNuevo.setEnabled(nuevo);
                btDiscograficaModificar.setEnabled(!nuevo);
                btDiscograficaEliminar.setEnabled(!nuevo);
                btDiscograficaGuardar.setEnabled(nuevo);

                seleccionarEditable(Tipo.DISCOGRAFICA, true);

                break;

            case DISCO:
                if (nuevo){
                    limpiarCajas(Tipo.DISCO);
                }

                tfDiscoTitulo.setEnabled(nuevo);
                cbDiscoArtista.setEnabled(nuevo);
                tfDiscoCopiasVendidas.setEnabled(nuevo);
                tfDiscoPrecioSalida.setEnabled(nuevo);
                cbDiscoCompania.setEnabled(nuevo);
                calDiscoFechaSalida.setEnabled(nuevo);

                btDiscoNuevo.setEnabled(nuevo);
                btDiscoModificar.setEnabled(!nuevo);
                btDiscoEliminar.setEnabled(!nuevo);
                btDiscoGuardar.setEnabled(nuevo);

                seleccionarEditable(Tipo.DISCO, true);

                break;

        }
    }

    //Al pulsar el botón guardar, todos los datos introducidos se almacenarán en un nuevo objeto que a la vez será guardado en
    //el vector. Destacar el control de errores, pues no debe haber ningún dato sin rellenar.
    public void guardarDatos(Tipo tipo, Boolean nuevo){
        Artista artistaseleccionado;
        Disco discoseleccionado;
        Discografica discograficaseleccionada;

        switch (tipo){
            case ARTISTA:
                Artista thisArtista;
                if (nuevo==true){
                    if (!tfArtistaNombre.getText().equals("") && !tfArtistaNombreOriginal.getText().equals("") && !tfArtistaNumeroTlfno.getText().equals("")
                            && !tfArtistaPrecioActuacion.getText().equals("") && !tfArtistaCiudadNacimiento.getText().equals("") && !calArtistaFechaNacimiento.getDate().equals(null)){
                        thisArtista = new Artista(tfArtistaNombre.getText(), tfArtistaNombreOriginal.getText(), Integer.parseInt(tfArtistaNumeroTlfno.getText()), Float.parseFloat(tfArtistaPrecioActuacion.getText()), tfArtistaCiudadNacimiento.getText(), calArtistaFechaNacimiento.getDate());
                        VectorArtistas.add(thisArtista);
                        limpiarCajas(Tipo.ARTISTA);
                        actualizarLista(Tipo.ARTISTA);
                        Util.MensajeDeAlerta("Artista añadido a la lista");
                        Util.GuardarFichero(VectorArtistas, "Vectores"+Separador+"Artistas.obj");
                    } else {
                        Util.MensajeDeAlerta("Error. Es necesario rellenar todos los datos");
                    }
                } else if (nuevo==false) {
                    int Indice = listArtista.getSelectedIndex();
                    VectorArtistas.remove(Indice);
                    if (!tfArtistaNombre.getText().equals("") && !tfArtistaNombreOriginal.getText().equals("") && !tfArtistaNumeroTlfno.getText().equals("")
                            && !tfArtistaPrecioActuacion.getText().equals("") && !tfArtistaCiudadNacimiento.getText().equals("") && !calArtistaFechaNacimiento.getDate().equals(null)) {
                        thisArtista = new Artista(tfArtistaNombre.getText(), tfArtistaNombreOriginal.getText(), Integer.parseInt(tfArtistaNumeroTlfno.getText()), Float.parseFloat(tfArtistaPrecioActuacion.getText()), tfArtistaCiudadNacimiento.getText(), calArtistaFechaNacimiento.getDate());
                        VectorArtistas.add(thisArtista);
                        limpiarCajas(Tipo.ARTISTA);
                        actualizarLista(Tipo.ARTISTA);
                        Util.GuardarFichero(VectorArtistas, "Vectores" + Separador + "Artistas.obj");
                        Util.MensajeDeAlerta("Artista modificado correctamente");
                    }
                }


                break;

            case DISCOGRAFICA:
                artistaseleccionado = (Artista)cbDiscograficaArtistaEstrella.getSelectedItem();
                Discografica thisDiscografica;

                if (nuevo==true){
                    if (!tfDiscograficaNombre.getText().equals("") && !tfDiscograficaNumeroArtistas.getText().equals("") && !tfDiscograficaCiudadSede.getText().equals("")
                            && !tfDiscograficaDineroRecaudado.getText().equals("") && artistaseleccionado!=null && !calDiscograficaFechaRegistro.getDate().equals(null)){
                        Tributa tributos = null;

                        if (rbDiscograficaSiTributa.isSelected()){
                            tributos = Tributa.Si;
                        } else {
                            tributos = Tributa.No;
                        }

                        thisDiscografica = new Discografica(tfDiscograficaNombre.getText(), Integer.parseInt(tfDiscograficaNumeroArtistas.getText()), tfDiscograficaCiudadSede.getText(), Float.parseFloat(tfDiscograficaDineroRecaudado.getText()), tributos, artistaseleccionado, calDiscograficaFechaRegistro.getDate());
                        VectorDiscograficas.add(thisDiscografica);
                        limpiarCajas(Tipo.DISCOGRAFICA);
                        actualizarLista(Tipo.DISCOGRAFICA);
                        Util.MensajeDeAlerta("Discográfica añadida a la lista");
                        Util.GuardarFichero(VectorDiscograficas, "Vectores"+Separador+"Discograficas.obj");
                    } else {
                        Util.MensajeDeAlerta("Error. Es necesario rellenar todos los datos");
                    }
                } else if (nuevo==false) {
                    int Indice = listDiscografica.getSelectedIndex();
                    VectorDiscograficas.remove(Indice);
                    if (!tfDiscograficaNombre.getText().equals("") && !tfDiscograficaNumeroArtistas.getText().equals("") && !tfDiscograficaCiudadSede.getText().equals("")
                            && !tfDiscograficaDineroRecaudado.getText().equals("") && artistaseleccionado != null && !calDiscograficaFechaRegistro.getDate().equals(null)) {
                        Tributa tributos = null;

                        if (rbDiscograficaSiTributa.isSelected()) {
                            tributos = Tributa.Si;
                        } else {
                            tributos = Tributa.No;
                        }

                        thisDiscografica = new Discografica(tfDiscograficaNombre.getText(), Integer.parseInt(tfDiscograficaNumeroArtistas.getText()), tfDiscograficaCiudadSede.getText(), Float.parseFloat(tfDiscograficaDineroRecaudado.getText()), tributos, artistaseleccionado, calDiscograficaFechaRegistro.getDate());
                        VectorDiscograficas.add(thisDiscografica);
                        limpiarCajas(Tipo.DISCOGRAFICA);
                        actualizarLista(Tipo.DISCOGRAFICA);
                        Util.GuardarFichero(VectorDiscograficas, "Vectores" + Separador + "Discograficas.obj");
                        Util.MensajeDeAlerta("Discográfica modificada correctamente");
                    }
                }

                break;

            case DISCO:

                if (nuevo==true){
                    artistaseleccionado = (Artista)cbDiscograficaArtistaEstrella.getSelectedItem();
                    discograficaseleccionada = (Discografica)cbDiscoCompania.getSelectedItem();

                    if (!tfDiscoTitulo.getText().equals("") && artistaseleccionado!=null && !tfDiscoCopiasVendidas.getText().equals("") && !tfDiscoPrecioSalida.getText().equals("")
                            && discograficaseleccionada!=null && !calDiscoFechaSalida.getDate().equals(null)){
                        Disco thisDisco;
                        thisDisco = new Disco(tfDiscoTitulo.getText(), artistaseleccionado, Integer.parseInt(tfDiscoCopiasVendidas.getText()), Float.parseFloat(tfDiscoPrecioSalida.getText()), discograficaseleccionada, calDiscoFechaSalida.getDate());
                        VectorDiscos.add(thisDisco);
                        limpiarCajas(Tipo.DISCO);
                        actualizarLista(Tipo.DISCO);
                        Util.MensajeDeAlerta("Disco añadido a la lista");
                        Util.GuardarFichero(VectorDiscos, "Vectores"+Separador+"Discos.obj");
                    } else {
                        Util.MensajeDeAlerta("Error. Es necesario rellenar todos los datos");
                    }
                } else if (nuevo==false) {
                    artistaseleccionado = (Artista) cbDiscograficaArtistaEstrella.getSelectedItem();
                    discograficaseleccionada = (Discografica) cbDiscoCompania.getSelectedItem();

                    if (!tfDiscoTitulo.getText().equals("") && artistaseleccionado != null && !tfDiscoCopiasVendidas.getText().equals("") && !tfDiscoPrecioSalida.getText().equals("")
                            && discograficaseleccionada != null && !calDiscoFechaSalida.getDate().equals(null)) {
                        Disco thisDisco;
                        thisDisco = new Disco(tfDiscoTitulo.getText(), artistaseleccionado, Integer.parseInt(tfDiscoCopiasVendidas.getText()), Float.parseFloat(tfDiscoPrecioSalida.getText()), discograficaseleccionada, calDiscoFechaSalida.getDate());
                        VectorDiscos.add(thisDisco);
                        limpiarCajas(Tipo.DISCO);
                        actualizarLista(Tipo.DISCO);
                        Util.GuardarFichero(VectorDiscos, "Vectores" + Separador + "Discos.obj");
                        Util.MensajeDeAlerta("Disco modificado correctamente");
                    }
                }

                break;
        }

    }

    //Método para cargar la lista al que se llamará cada vez que hagamos un cambio sobre ella
    public void actualizarLista(Tipo tipo){
        switch (tipo){

            case ARTISTA:
                DefaultListModel<Artista> modelArtistas = new DefaultListModel<>();
                for (int i=0; i < VectorArtistas.size(); i++){
                    modelArtistas.addElement(VectorArtistas.get(i));
                    cbDiscoArtista.addItem(VectorArtistas.get(i));
                    cbDiscograficaArtistaEstrella.addItem(VectorArtistas.get(i));
                }
                listArtista.setModel(modelArtistas);

                break;

            case DISCOGRAFICA:
                DefaultListModel<Discografica> modelDiscograficas = new DefaultListModel<>();
                for (int i=0; i < VectorDiscograficas.size(); i++){
                    modelDiscograficas.addElement(VectorDiscograficas.get(i));
                    cbDiscoCompania.addItem(VectorDiscograficas.get(i));
                }
                listDiscografica.setModel(modelDiscograficas);

                break;

            case DISCO:
                DefaultListModel<Disco> modelDiscos = new DefaultListModel<>();
                for (int i=0; i < VectorDiscos.size(); i++){
                    modelDiscos.addElement(VectorDiscos.get(i));
                }
                listDisco.setModel(modelDiscos);

                break;
        }

    }

    //Al pulsar el botón modificar se podrá escribir y editar sobre las cajas de texto, si no se pulsa no serán editables
    public void modificarDatos(Tipo tipo){

        switch (tipo){

            case ARTISTA:
                seleccionarEditable(Tipo.ARTISTA, true);
                nuevoArtista=false;

                break;

            case DISCOGRAFICA:
                seleccionarEditable(Tipo.DISCOGRAFICA, true);
                nuevoDiscografica=false;

                break;

            case DISCO:
                seleccionarEditable(Tipo.DISCO, true);
                nuevoDisco=false;

                break;
        }
    }

    //Al pulsar el botón eliminar, se eliminará el objeto del vector, se actualizará la lista y se limpiarán todas las cajas
    public void eliminarDatos(Tipo tipo, int Indice){
        switch (tipo){

            case ARTISTA:
                int respuestaArtista = Util.MensajeDeConfirmacion("¿Estás seguro de querer eliminar a "+VectorArtistas.get(Indice).getNombreArtistico()+"?");
                if (respuestaArtista == JOptionPane.YES_OPTION){
                    VectorArtistas.remove(Indice);
                    Util.GuardarFichero(VectorArtistas, "Vectores"+Separador+"Artistas.obj");
                    actualizarLista(Tipo.ARTISTA);
                    limpiarCajas(Tipo.ARTISTA);
                    Util.MensajeDeAlerta("El artista ha sido eliminado correctamente");
                } else {
                    Util.MensajeDeAlerta("El artista no ha sido eliminado finalmente");
                }

                break;

            case DISCOGRAFICA:
                int respuestaDiscografica = Util.MensajeDeConfirmacion("¿Estás seguro de querer eliminar a "+VectorDiscograficas.get(Indice).getNombreDiscografica()+"?");
                if (respuestaDiscografica == JOptionPane.YES_OPTION){
                    VectorDiscograficas.remove(Indice);
                    Util.GuardarFichero(VectorDiscograficas, "Vectores"+Separador+"Discograficas.obj");
                    actualizarLista(Tipo.DISCOGRAFICA);
                    limpiarCajas(Tipo.DISCOGRAFICA);
                    Util.MensajeDeAlerta("La discográfica ha sido eliminada correctamente");
                } else {
                    Util.MensajeDeAlerta("La discográfica no ha sido eliminado finalmente");
                }

                break;

            case DISCO:
                int respuestaDisco = Util.MensajeDeConfirmacion("¿Estás seguro de eliminar "+VectorDiscos.get(Indice).getTituloDisco()+"?");
                if (respuestaDisco == JOptionPane.YES_OPTION){
                    VectorDiscos.remove(Indice);
                    Util.GuardarFichero(VectorDiscos, "Vectores"+Separador+"Discos.obj");
                    actualizarLista(Tipo.DISCO);
                    limpiarCajas(Tipo.DISCO);
                    Util.MensajeDeAlerta("El disco ha sido eliminado correctamente");
                } else {
                    Util.MensajeDeAlerta("El disco no ha sido eliminado finalmente");
                }

                break;
        }

    }

}
